(sde:clear)

(define TSUB 50)

(define Lele_pitchX @PitchX@)
(define Lal_eleX @AlSizeX@)

(define Lele_pitchY @PitchY@)
(define Lal_eleY @AlSizeY@)


(define NX @NX@)
(define NY @NY@)

(define NDCX @NDCX@)
(define NDCY @NDCY@)

(define LX (* NX Lele_pitchX))
(define LY (* NY Lele_pitchY))

(define Plow 1e12)
(define Nca0 0)
(define Nca @nplus_dope@)
(define Pan 1e19)
(define Xj @nplus_dep@)

(define p_layer_Peak		@pplus_peak@)
(define p_layer_Depth		@pplus_width@)

(define whi 0.1)
(define Lhi 50)
(define p_well_doping_concentration @pplus_dope@) ; 3e16
(define al_dep 0.3)

(define edgesize 250)
(define GRsize 100)
(define BRsize 100)

(sdegeo:create-cuboid (position 0 (- 0 edgesize) (- 0 edgesize)) (position TSUB (+ LX edgesize) (+ LY edgesize)) "Silicon" "RSub")



(define mpolynp (list))

(define ee (list	
;;	(+ (* 0 Lele_pitchX) (* 0.5 (- Lele_pitchX Lal_eleX))) (+ (* 0 Lele_pitchY) (* 0.5 (- Lele_pitchY Lal_eleY)))
;;	(+ (* (- NX 1) Lele_pitchX) (* 0.5 (+ Lele_pitchX Lal_eleX))) (+ (* 0 Lele_pitchY) (* 0.5 (- Lele_pitchY Lal_eleY)))
;;	(+ (* (- NX 1) Lele_pitchX) (* 0.5 (+ Lele_pitchX Lal_eleX))) (+ (* (- NY 1) Lele_pitchY) (* 0.5 (+ Lele_pitchY Lal_eleY)))
;;	(+ (* 0 Lele_pitchX) (* 0.5 (- Lele_pitchX Lal_eleX))) (+ (* (- NY 1) Lele_pitchY) (* 0.5 (+ Lele_pitchY Lal_eleY)))
	0 0 LX LY
))
(set! mpolynp (append mpolynp (list ee)))

(sdepe:generate-mask "EE" mpolynp )

(sdepe:pattern "mask" "EE" "polarity" "dark" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )


(sdedr:define-gaussian-profile "Nplus" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Nca "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)
(sdepe:implant "Nplus" "flat")

(entity:delete (find-material-id "Resist"))

;; adding bias ring (DC ring)
(define fflist (list))
(define ff (list (- 0 BRsize) (- 0 BRsize)  (+ LX BRsize) (+ LY BRsize)))
(set! fflist (append fflist (list ff)))
(sdepe:generate-mask "BRO" fflist )
(sdepe:pattern "mask" "BRO" "polarity" "dark" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep")

(define gglist (list))
(define gg (list (+ 0 (* Lele_pitchX NDCX)) (+ 0 (* Lele_pitchY NDCY)) (- LX (* Lele_pitchX NDCX)) (- LY (* Lele_pitchY NDCY))))
(set! gglist (append gglist (list gg)))

(sdepe:generate-mask "BRI" gglist )
(sdepe:pattern "mask" "BRI" "polarity" "light" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep")

(sdedr:define-gaussian-profile "Npp" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" (* Xj 2) "Gauss"  "Factor" 0.8)
(sdepe:implant "Npp" "flat")

(entity:delete (find-material-id "Resist"))

;; addeing Gard Ring 
(define iilist (list))
(define ii (list (+ (- 0 edgesize) GRsize) (+ (- 0 edgesize) GRsize)  (+ LX (- edgesize GRsize)) (+ LY (- edgesize GRsize))))
(set! iilist (append iilist (list ii)))
(sdepe:generate-mask "GR" iilist )
(sdepe:pattern "mask" "GR" "polarity" "light" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep")

(sdedr:define-gaussian-profile "GardRing" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)
(sdepe:implant "GardRing" "flat")

(entity:delete (find-material-id "Resist"))


(define ee (entity:copy (find-mask "EE")))
(sde:attrib-remove ee "maskname")
(generic:add ee "maskname" "EE2")
(sde:offset-mask "EE2" -30)
(sdepe:pattern "mask" "EE2" "polarity" "dark" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )

;;(sdedr:define-gaussian-profile "Pplus" "BoronActiveConcentration" "PeakPos" p_layer_Peak "PeakVal" p_well_doping_concentration "ValueAtDepth" (/ p_well_doping_concentration 10) "Depth" p_layer_Depth "Gauss" "Factor" 1)

(sdedr:define-refeval-window "Pplus" "Rectangle" (position p_layer_Peak (+ 0 (* Lele_pitchX NDCX)) (+ 0 (* Lele_pitchY NDCY))) 
			                         (position p_layer_Peak (- LX (* Lele_pitchX NDCX)) (- LY (* Lele_pitchX NDCX))))
(sdedr:define-gaussian-profile "Pplus" "BoronActiveConcentration" "PeakPos" 0 "PeakVal" p_well_doping_concentration "ValueAtDepth" Plow "Depth" p_layer_Depth "Gauss" "Factor" 1)

;(sdepe:implant "Pplus" "flat")
(sdedr:define-analytical-profile-placement "Pplus" "Pplus" "Pplus" "Both" "NoReplace" "Eval")


(entity:delete (find-material-id "Resist"))

(define mpolydcal (list))

(   
do ((i 0 (+ i 1))) ((>= i NY))
	(
	do ((j 0 (+ j 1))) ((>= j NX))
	        (define isDCelec #f)
		(if (< j NDCX) (set! isDCelec #t))
		(if (> j (- NX (+ NDCX 1))) (set! isDCelec #t))
		(if (< i NDCY) (set! isDCelec #t))
		(if (> i (- NY (+ NDCY 1))) (set! isDCelec #t))

		(if (eq? isDCelec #t)
                   (begin
		  (define cc (list 
				(+ (* j Lele_pitchX) (* 0.5 (- Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (- Lele_pitchY Lal_eleY)))
				(+ (* j Lele_pitchX) (* 0.5 (+ Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (- Lele_pitchY Lal_eleY)))
				(+ (* j Lele_pitchX) (* 0.5 (+ Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (+ Lele_pitchY Lal_eleY)))
				(+ (* j Lele_pitchX) (* 0.5 (- Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (+ Lele_pitchY Lal_eleY)))
		  ))
	          (set! mpolydcal (append mpolydcal (list cc)))
             )
         )
    )
)

(define brpad (list))
(define kk (list -75 -75 -25 -25))
(set! mpolydcal (append mpolydcal (list kk)))

(sdepe:generate-mask "CC" mpolydcal )
(sdepe:pattern "mask" "CC" "polarity" "light" "material" "Aluminum" "thickness" 0.1  "type" "aniso" "algorithm" "sweep" )

(sdepe:fill-device "material" "Oxide")


(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat1_DCal.tdr")

(define mpolyal (list))
(
do ((i 0 (+ i 1))) ((>= i NY))
	(
	do ((j 0 (+ j 1))) ((>= j NX))
		(define ff (list 
				(+ (* j Lele_pitchX) (* 0.5 (- Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (- Lele_pitchY Lal_eleY)))
				(+ (* j Lele_pitchX) (* 0.5 (+ Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (- Lele_pitchY Lal_eleY)))
				(+ (* j Lele_pitchX) (* 0.5 (+ Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (+ Lele_pitchY Lal_eleY)))
				(+ (* j Lele_pitchX) (* 0.5 (- Lele_pitchX Lal_eleX))) (+ (* i Lele_pitchY) (* 0.5 (+ Lele_pitchY Lal_eleY)))
		))
	(set! mpolyal (append mpolyal (list ff)))
	)
)

(sdepe:generate-mask "DD" mpolyal )


(sdepe:pattern "mask" "DD" "polarity" "light" "material" "Aluminum" "thickness" 0.3  "type" "aniso" "algorithm" "sweep" )

(sdepe:fill-device "material" "Oxide")


(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat3_al.tdr")

(bool:unite (find-material-id "Oxide"))
(bool:unite (find-material-id "Aluminum"))
(sde:separate-lumps)


(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat4_unite.tdr")

(
do ((i 0 (+ i 1))) ((>= i NY))
	(
	do ((j 0 (+ j 1))) ((>= j NX))
		(sde:add-material (find-body-id 
			(position -0.4 (+ (* j Lele_pitchX) (* 0.5 Lele_pitchX)) (+ (* i Lele_pitchY) (* 0.5 Lele_pitchY))))
		"Aluminum" (string-append "RAl" (number->string j) (number->string i)))
		
		(sdegeo:define-contact-set (string-append "cathode" (number->string j) (number->string i)) 4  (color:rgb 1 0 0 ) "##")

		(sdegeo:set-current-contact-set (string-append "cathode" (number->string j) (number->string i)) )
		(sdegeo:set-contact-boundary-faces (find-region-id (string-append "RAl" (number->string j) (number->string i))))

	)
)




(sdedr:define-refeval-window "RefEval_anode" "Rectangle"  (position TSUB (- 0 edgesize) (- 0 edgesize)) (position TSUB (+ LX edgesize) (+ LY edgesize)))

(sdedr:define-gaussian-profile "DopAnode" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" -5 "Gauss"  "Factor" 0.8)

(sdedr:define-analytical-profile-placement "Place_anode" "DopAnode" "RefEval_anode" "Both" "NoReplace" "Eval")



(sdegeo:define-contact-set "anode" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:set-current-contact-set "anode")
(sdegeo:set-contact-faces (find-face-id (position TSUB 0.1 0.1)))

(entity:delete (find-material-id "Aluminum"))

(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat5.tdr")

(sdedr:define-constant-profile "ConstantProfileDefinition_1" "BoronActiveConcentration" Plow)
(sdedr:define-constant-profile-material "ConstantProfilePlacement_1" "ConstantProfileDefinition_1" "Silicon")

(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat6.tdr")

(sdedr:define-refinement-size "RefinementDefinition_1" (/ TSUB 10.0) (* 0.1 LX) (* 0.1 LY) 0.2 0.5 0.5 )
;;(sdedr:define-refinement-size "RefinementDefinition_1" 1 1 1 0.2 0.5 0.5 )
(sdedr:define-refinement-placement "RefinementPlacement_1" "RefinementDefinition_1" (list "material" "Silicon" ) )
(sdedr:define-refinement-function "RefinementDefinition_1" "DopingConcentration" "MaxTransDiff" 1)


(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat7.tdr")

(define takemax (lambda(ll) (list-ref (sort > ll) 0)	))

	
(define create-polyhedron-refine (lambda(name posx posy posz vv TSUB L)
	
		(sdegeo:set-default-boolean "XX")
		(define pdisk (face:planar-disk (position posx posy posz) vv TSUB))
		(extract-refwindow pdisk "jj")
		
		(define hh (sdegeo:chull2d (map vertex:position (entity:vertices (find-drs-id "jj")) )  ))
		(sdedr:define-refeval-window name "Polygon" hh  )

		(sdedr:define-refeval-window "Ref_fakeprofile" "Polygon" hh  )
		
		(sdedr:define-erf-profile "fakeprofile" "PMIUserField99" "SymPos" L  "MaxVal" 1 "ValueAtDepth" 0 "Depth" (+ L (* 0.5 TSUB)) "Erf"  "Length" (* 0.5 TSUB))
		(sdedr:define-analytical-profile-placement "Place_fakeprofile" "fakeprofile" "Ref_fakeprofile" "Both" "NoReplace" "Eval")

		(define path5 (sdegeo:create-polyline-wire (list 
					(position posx posy posz)
					(position (+ posx (* L (gvector:x vv))) (+ posy (* L (gvector:y vv))) (+ posz (* L (gvector:z vv))))  
				)
			)
		)
		
		(sdegeo:sweep (find-drs-id name) path5  (sweep:options "solid" #t "rigid" #f "miter_type" "default"  ))

		(sdedr:define-refinement-size (string-append "DEF_" name) 
			(* 0.1 (takemax (list (* (abs (gvector:z vv)) TSUB) (* (abs (gvector:y vv)) TSUB) (* (abs (gvector:x vv)) L))     )   )
			(* 0.1 (takemax (list (* (abs (gvector:x vv)) TSUB) (* (abs (gvector:z vv)) TSUB) (* (abs (gvector:y vv)) L))     )   )
			(* 0.1 (takemax (list (* (abs (gvector:y vv)) TSUB) (* (abs (gvector:x vv)) TSUB) (* (abs (gvector:z vv)) L))     )   )
			0.1 
			0.1 
			0.1 		)
		(sdedr:define-refinement-function (string-append "DEF_" name) "PMIUserField99" "MaxTransDiff" 0.1)
		(sdedr:define-refinement-placement (string-append "PLACE_" name) (string-append "DEF_" name) (list "window" name ) )
		(entity:delete (find-drs-id "jj"))

))





(sdeio:save-tdr-bnd (get-body-list) "n@node@_pat8.tdr")



(sdegeo:translate-selected (get-body-list) (transform:translation (gvector  0 (- 0 (* 0.5 LX)) (- 0 (* 0.5 LY)))) #f 0)
(sdegeo:translate-selected (get-drs-list) (transform:translation (gvector  0 (- 0 (* 0.5 LX)) (- 0 (* 0.5 LY)))) #f 0)


;;(create-polyhedron-refine "REFION" 0 @posX@ @posY@ (gvector @dirX@ @dirY@ @dirZ@) whi Lhi)
;;(create-polyhedron-refine "REFION" 0 0 TSUB (gvector  0 0 -1) whi Lhi)

(sdedelaunizer:set-parameters "maxPoints" 10000000  "delaunayTolerance" 0.001)

(sde:build-mesh "snmesh" " " "n@node@_msh")







