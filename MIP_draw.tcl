#/home/kojin/work/Sentaurus/SimulationTCAD/2D/LGAD/LGADStrip/MIP_draw.tcl
#MAKO: Output from Tcl commands history.

set fp [open gtree.dat r ]
set file_data [read $fp]
close $fp

set lines [split $file_data "\n"]

set alw 0
set xpos 0
set ndep 0
set ndope 0
set pdope 0

set i 0
foreach line $lines {
    set linedata [split $line {" "}]

    if { [lindex $linedata 0 ] == 3 } {
	set alw [lindex $linedata 3]
	set alw [lindex [split $alw "{"] 1]
	set alw [lindex [split $alw "}"] 0]
#	puts "$alw"
    }

    if { [lindex $linedata 0 ] == 4 } {
	set xpos [lindex $linedata 3]
	set xpos [lindex [split $xpos "{"] 1]
	set xpos [lindex [split $xpos "}"] 0]
#	puts "$xpos"
    }

    if { [lindex $linedata 0 ] == 9 } {
	set ndep [lindex $linedata 3]
	set ndep [lindex [split $ndep "{"] 1]
	set ndep [lindex [split $ndep "}"] 0]
#	puts "$ndep"
    }


    if { [lindex $linedata 0 ] == 10 } {
	set ndope [lindex $linedata 3]
	set ndope [lindex [split $ndope "{"] 1]
	set ndope [lindex [split $ndope "}"] 0]
#	puts "$ndep"
    }

    if { [lindex $linedata 0 ] == 11 } {
	set pdope [lindex $linedata 3]
	set pdope [lindex [split $pdope "{"] 1]
	set pdope [lindex [split $pdope "}"] 0]
#	puts "$ndep"
    }

    if { [lindex $linedata 0 ] == 16 } {
	set node "n[lindex $linedata 1]"
	puts "$node $i"
	puts "al${alw}_ndep${ndep}_n${ndope}_p${pdope}_x${xpos}"
	if { [file exists MIP_${node}_des.plt ] == 0 } { continue }
	load_file MIP_${node}_des.plt
	create_plot -1d -name Plot_$i
	select_plots Plot_$i
	#-> Plot_1
	#-> Plot_1
	
	#create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {anode TotalCurrent}
	#create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode1 TotalCurrent}
	create_curve -name Curve_1 -plot Plot_$i -dataset MIP_${node}_des -axisX time -axisY {cathode33 TotalCurrent}
	create_curve -name Curve_2 -plot Plot_$i -dataset MIP_${node}_des -axisX time -axisY {cathode32 TotalCurrent}
	create_curve -name Curve_3 -plot Plot_$i -dataset MIP_${node}_des -axisX time -axisY {cathode31 TotalCurrent}
	create_curve -name Curve_4 -plot Plot_$i -dataset MIP_${node}_des -axisX time -axisY {cathode22 TotalCurrent}
	#create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode7 TotalCurrent}
	set_plot_prop -plot Plot_$i -hide_title
	set_axis_prop -plot Plot_$i -axis x -min 4e-09 -min_fixed
	set_axis_prop -plot Plot_$i -axis x -max 1e-08 -max_fixed
	#-> 0
	
	set_legend_prop -plot Plot_$i -position {0.6 0.8}
	
	export_curves {Curve_1 Curve_2 Curve_3 Curve_4 Curve_5} -plot Plot_$i -filename MIPSig_al${alw}_ndep${ndep}_n${ndope}_p${pdope}_x${xpos}_data.csv -format csv -overwrite
	remove_plots Plot_$i
	
	incr i
    }
}

