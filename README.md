# ACLGADPixel3D
## before starting
### Sentaurus version requirement

    This program developed by Q_2019.12-SP1a version
    Highly recommened to use Q_2019.12-SP1a or later version

### How to run jobs

    1. using work bench

    git clone https://gitlab.cern.ch/kojin/ACLGADPixel3D.git ACLGADPixel3D-master
    cd ACLGADPixel3D-master
    swb . &

    select node whatever you want

    2. using command line (example)

    git clone https://gitlab.cern.ch/kojin/ACLGADPixel3D.git ACLGADPixel3D-master
    cd ACLGADPixel3D-master 
    spp . 
    sde -e -l  n1252_dvs.cmd 
    gjob -verbose -job 1187 .
