#!/bin/bash


if [ ! $2 ]
then
    echo "usage : $0 [node#1] [node#2] ... "
    exit
fi


function timediff (){
    time1=$1
    time2=$2
#    tdiff=`echo "$time2 - $time1 " | bc -l`
#    echo $tdiff
#
#    echo -n " "`echo  "int($tdiff / 3600)" | bc -l `
#    echo -n " "`echo  "$tdiff % 3600" | bc -l `
#    echo -n " "`echo " $tdiff % 60 " | bc -l `
#    echo -n " "`echo " $tdiff " | bc -lls`
#    echo ""

    let deltatime=time2-time1
    let hours=deltatime/3600
    let minutes=(deltatime/60)%60
    let seconds=deltatime%60
    echo $hours"h "$minutes"m "$seconds"s"
}

for ii in $@
do
    NODE=$ii
    
    
    if [ -f "pp${NODE}_dvs.cmd" ]
    then
	STARTTIME=`ls --time-style='+%s' -l pp${NODE}_dvs.cmd | awk '{print $6}' `
	BEFORMESH=`ls --time-style='+%s' -l n${NODE}_msh.cmd | awk '{print $6}' `
	ENDTIME=`ls --time-style='+%s' -l n${NODE}_dvs.sta | awk '{print $6}' `
	echo "SDE full job length (mesh): "`timediff $STARTTIME $ENDTIME`" ("`timediff $BEFORMESH $ENDTIME`")"
	
    elif [ -f "pp${NODE}_des.cmd" ]
    then
	STARTTIME=`ls --time-style='+%s' -l pp${NODE}_des.cmd | awk '{print $6}' `
	ENDTIME=`ls --time-style='+%s' -l n${NODE}_des.sta | awk '{print $6}' `
	echo -n "full job length : "
	timediff $STARTTIME $ENDTIME
    else
	echo "preprocess file not found : pp${NODE}_*.cmd"
    fi
done
